package com.example.android.musicplayer;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.VolumeProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.R.attr.button;
import static android.R.attr.onClick;
import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class MainActivity extends AppCompatActivity {
    //create global variable with MediaPlayer object
    private MediaPlayer mediaPlayer;
    //create global variable with AudioManager object
    private AudioManager audioManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize the mediaPlayer object with siewca.mp3 song
        mediaPlayer = MediaPlayer.create(this, R.raw.siewca);
        //Initialize AudioManager object with to manage the audio settings
        audioManager = (AudioManager) getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        //Find play button View
        Button play = (Button) findViewById(R.id.btn_play);
        //Find pause button View
        Button pause = (Button) findViewById(R.id.btn_pause);

        //set onClick Listener to play the music
        play.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Perform action on click
                mediaPlayer.start();
            }
        });
        //set onClick Listener to pause the music
        pause.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Perform action on click
                mediaPlayer.pause();
            }
        });

        //Find +vol button
        Button plus_vol = (Button) findViewById(R.id.btn_plus_volume);
        //Find -vol button
        Button minus_vol = (Button) findViewById(R.id.btn_minus_volume);

        //set onClick Listener to increase the volume of the music
        plus_vol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast.makeText(MainActivity.this, "Increasing the volume", Toast.LENGTH_LONG).show();
                //To increase media player volume
                audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
            }
        });
        //set onClick Listener to decrease the volume of the music
        minus_vol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Decreasing the volume", Toast.LENGTH_LONG).show();
                //To decrease media player volume
                audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
            }
        });
    }
}

